# Hermes Benchmark

# How to build?

## 1. Install environment

Environment requires:

- CMAKE: at least 3.10

### Fedora

You can setup using:

```bash
sudo dnf install cmake g++ gcc make -y
# Install openmpi, or you can use another MPI library, e.g. https://www.intel.com/content/www/us/en/developer/tools/oneapi/mpi-library.html
sudo dnf install openmpi openmpi-devel -y
# Each time you need to use mpi, you need to source the environment
source /etc/profile.d/modules.sh
module load mpi/openmpi-x86_64
```

### Ubuntu

You can setup using:

```bash
sudo apt install cmake make gcc g++ -y
# Install openmpi, or you can use another MPI library, e.g. https://www.intel.com/content/www/us/en/developer/tools/oneapi/mpi-library.html
sudo apt install libopenmpi-dev
```

## 2. Create the build dir

In source builds are forbidden to keep git tree clean.
`build` directory is ignored, so you can use that one to compile files

```bash
mkdir -p build
cd build
cmake ..
make
```

## 3. Install

You can also install the binaries if you wish.
Make sure you set the prefix (otherwise `/usr/local` will be used).

```bash
cmake .. -DPREFIX=/my/location
make
make install
```

This will install the binary files into `/my/location/bin`.

## 4. Run the benchmark

```bash
mpirun -n 4 <other_params> ./build/incast_bench # or use only the benchmark name if you installed it
```

# Testing

You can find test related files in `test` directory.
When compiling, you need to enable testing to get the test binaries.
Due to single header architecture, we generate different test binaries for different test modules.
You can run the tests:

```bash
cmake -DBUILD_TESTS=ON ..
make
mpirun -n 4 ./test_synchronizer
```
