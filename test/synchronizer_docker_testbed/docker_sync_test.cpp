#include "hermes.hpp"
#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include <arpa/inet.h>
#include <iomanip>
#include <limits>
#include <mpi.h>
#include <net/if.h>
#include <netinet/in.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <unistd.h>

/*
Run this test with big network delays on the distributed network nodes.
This can be done using tc (traffic control) on Linux.

*/
#include "gmock/gmock.h"
#include <mpi.h>

int main(int argc, char *argv[]) {
    int result = 0;

    ::testing::InitGoogleTest(&argc, argv);
    hermes::init(argc, argv, hermes::default_config);
    result = RUN_ALL_TESTS();
    hermes::finalize();

    return result;
}

#define SERVER_IP "192.168.228.1"
#define PORT 8080
#define BUFFER_SIZE 1024

using namespace hermes::time;
static int sockfd;
void send_string(const char *buffer) {
    /*
    Send a string to the server so that the server saves the timestamp.
    */
    size_t len = strlen(buffer);

    // Send redundant messages because they aredropped
    ssize_t bytes_sent = send(sockfd, buffer, strlen(buffer), 0);
    if (bytes_sent < 0) {
        perror("send");
        close(sockfd);
        exit(EXIT_FAILURE);
    }
}

void create_socket() {
    /*
    Make connecction to the fast server.
    Using this server, we record the timestamps.
    */
    struct sockaddr_in server_addr;

    // Create socket
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) {
        perror("socket");
        exit(EXIT_FAILURE);
    }

    // Set up server address
    memset(&server_addr, 0, sizeof(server_addr));
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(PORT);

    if (inet_pton(AF_INET, SERVER_IP, &server_addr.sin_addr) <= 0) {
        perror("inet_pton");
        close(sockfd);
        exit(EXIT_FAILURE);
    }

    // Connect to the server
    if (connect(sockfd, (struct sockaddr *)&server_addr, sizeof(server_addr)) < 0) {
        perror("connect");
        close(sockfd);
        exit(EXIT_FAILURE);
    }

    send_string("starting...");
}
void delete_socket() {
    close(sockfd);
}

std::string get_ip_address() {
    /*
    Just for information (which core runs on which machine etc.).
    */
    int fd;
    struct ifreq ifr;

    // Create a socket
    fd = socket(AF_INET, SOCK_DGRAM, 0);
    if (fd == -1) {
        perror("socket");
        exit(EXIT_FAILURE);
    }

    // Copy the interface name to ifr structure
    strncpy(ifr.ifr_name, "eth0", IFNAMSIZ - 1);
    ifr.ifr_name[IFNAMSIZ - 1] = '\0';

    // Perform the ioctl system call to get the IP address
    if (ioctl(fd, SIOCGIFADDR, &ifr) == -1) {
        perror("ioctl");
        close(fd);
        exit(EXIT_FAILURE);
    }

    // Get the IP address from the ifr structure
    struct sockaddr_in *ipaddr = (struct sockaddr_in *)&ifr.ifr_addr;
    printf("IP address for %s: %s\n", "eth0", inet_ntoa(ipaddr->sin_addr));

    // Close the socket
    close(fd);

    return std::string(inet_ntoa(ipaddr->sin_addr));
}

static void sync_benchmark(int barrier_to_use, int iter) {
    /*
   Use sync and check the results.
   The result time should be more or less the same in microseconds.

   @param barrier_to_use: if 1, it uses MPI_Barrier, if 0, it uses synchronizer.sync()
   */

    global_clock_barrier synchronizer(MPI_COMM_WORLD, 5, 2);

    // The code below did not produce any meaningful results because they all show the same
    // But maybe they show something better with more nodes on TCP
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    // With the slow network, code below takes so much time
    // Therefore, we can use SSH to record the timestamp
    // This will send a message over the network
    // Actually, it is so slow
    // But since we can slow down the above MPI code, the network speed becomes relatively fast
    if (1 == barrier_to_use) {
        MPI_Barrier(MPI_COMM_WORLD);
    } else if (0 == barrier_to_use) {
        synchronizer.sync();
    } else {
        // No barrier
        int ra = random() % 100;
        usleep(ra * 1000);
    }
    // double now = MPI_Wtime();
    // fprintf(fp, "%d,%d,%.17g\n", rank, iter, now);
    char buffer[BUFFER_SIZE];
    sprintf(buffer, "after,%d,%d,%d", rank, barrier_to_use, iter);
    send_string(buffer);

    // std::cout << rank << std::setprecision(std::numeric_limits<double>::max_digits10) << now << std::endl;
}
namespace {
TEST(global_clock_barrier, sync_benchmark) {
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Barrier(MPI_COMM_WORLD);

    // Here we enable create a file so that the script can know we reached here
    // And then it can bind the eth1 to the network
    // This way, MPI will not be able to use eth1 when it's initialized since it does not exist
    system("touch mpi_server_ready_please_add_fast_network");
    usleep(1000 * 1000 * 10);

    usleep(10000 * rank);
    create_socket();

    usleep(10000 * rank);
    std::string a = std::string("#") + get_ip_address() + "," + std::to_string(rank);
    send_string(a.c_str());
    int number_of_times = 10;
    std::cout << "Sync benchmark MPI barrier" << std::endl;
    for (int i = 0; i < number_of_times; i++) {
        sync_benchmark(1, i);
        MPI_Barrier(MPI_COMM_WORLD);
    }
    MPI_Barrier(MPI_COMM_WORLD);
    std::cout << "Sync benchmark synchronizer sync" << std::endl;
    for (int i = 0; i < number_of_times; i++) {
        sync_benchmark(0, i);
        MPI_Barrier(MPI_COMM_WORLD);
    }
    MPI_Barrier(MPI_COMM_WORLD);
    std::cout << "Sync benchmark no barrier" << std::endl;
    for (int i = 0; i < number_of_times; i++) {
        sync_benchmark(2, i);
        MPI_Barrier(MPI_COMM_WORLD);
    }
    MPI_Barrier(MPI_COMM_WORLD);

    if (rank != 0) {
        send_string("end");
    }
    MPI_Barrier(MPI_COMM_WORLD);
    if (rank == 0) {
        send_string("end_all");
    }

    delete_socket();
}
TEST(global_clock_barrier, test_get_global_clock_diff) {
    MPI_Barrier(MPI_COMM_WORLD);
    global_clock_barrier synchronizer(MPI_COMM_WORLD, 10, 10);
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    double rtt = synchronizer.get_global_clock_diff();
    std::cout << "RTT between the root node and rank " << rank << " = " << rtt << std::endl;

    MPI_Barrier(MPI_COMM_WORLD);
}
} // namespace