#################### Setup the environment etc. ####################
docker network inspect slow_network || docker network create --subnet=192.168.228.0/24 slow_network
cd test/slow_docker_tests/setup_docker && docker images -a | grep my-ubuntu-image || docker build -t my-ubuntu-image .
cd ../../../

source /opt/intel/oneapi/setvars.sh
rm -rf build
mkdir -p build/output
cd test/slow_docker_tests/fast_server
make
cd ../../../
killall mpi_server
./test/slow_docker_tests/fast_server/mpi_server &

#################### Stop the test and clear the data ####################
docker container stop fatih_container1
docker container stop fatih_container2
docker container stop fatih_container3
# Clean containers
docker container rm fatih_container1
docker container rm fatih_container2
docker container rm fatih_container3

#################### Run the test ####################
docker run --privileged=true --cap-add=NET_ADMIN --cpus=1 -d -v "$(pwd)":/app/source/ --name fatih_container1 --net slow_network --ip 192.168.228.11 my-ubuntu-image bash -c " \
    /usr/sbin/sshd -D & \
    sleep 10 && \
    ssh-keyscan -H 192.168.228.11 >> ~/.ssh/known_hosts && \
    ssh-keyscan -H 192.168.228.12 >> ~/.ssh/known_hosts && \
    ssh-keyscan -H 192.168.228.13 >> ~/.ssh/known_hosts && \
    source /opt/intel/oneapi/setvars.sh && \
    cd /app/source && \
    cd build && \
    cmake -DBUILD_TESTS=ON .. && \
    make -j4 && \
    sudo tc qdisc add dev eth0 root handle 1: prio && \
    sudo tc filter add dev eth0 protocol ip parent 1:0 prio 1 u32 match ip dst 192.168.228.12/32 flowid 1:1 && \
    sudo tc filter add dev eth0 protocol ip parent 1:0 prio 2 u32 match ip dst 192.168.228.13/32 flowid 1:2 && \
    sudo tc qdisc add dev eth0 parent 1:1 handle 10: netem delay 1ms && \
    sudo tc qdisc add dev eth0 parent 1:2 handle 20: netem delay 1.5ms && \
    ping 192.168.228.12 -c 4 && \
    ping 192.168.228.13 -c 4 && \
    I_MPI_DEBUG=4 I_MPI_FABRICS=ofi I_MPI_OFI_PROVIDER=tcp  I_MPI_HYDRA_IFACE=eth0 ICX_TLS=tcp \
    mpiexec -iface eth0 -machinefile /app/source/test/slow_docker_tests/machinefile \
    -hostfile /app/source/test/slow_docker_tests/hostfile \
    -n 3 /app/source/build/test_slow_sycnhronizer"
docker run --cpus=1 -d -v "$(pwd)":/app/source/ --name fatih_container2 --net slow_network --ip 192.168.228.12 my-ubuntu-image bash -c "/usr/sbin/sshd -D & sleep 3600"
docker run --cpus=1 -d -v "$(pwd)":/app/source/ --name fatih_container3 --net slow_network --ip 192.168.228.13 my-ubuntu-image bash -c "/usr/sbin/sshd -D & sleep 3600"
docker logs -f fatih_container1

#################### Stop the test and clear the data ####################
# Stop other containers
docker container stop fatih_container2
docker container stop fatih_container3
# Clean containers
docker container rm fatih_container1
docker container rm fatih_container2
docker container rm fatih_container3
pip3 install matplotlib numpy
python3 scripts/synchronizer/visualize_the_errors_difference.py
