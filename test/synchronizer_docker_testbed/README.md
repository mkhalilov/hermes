# Delay Test

Some tests cannot determine if a function works as expected.
For example, syncronization test requires instant data communication to make sure it works.
To avoid this, we assume network is instant by increasing the network delays of the MPI to high levels.
The tests in this folder runs under very high network delays so that the delays required in the instant checks are negligible.

In theory, we have 3 docker images which run the program.
One is the root where we start the program.
The others are clients.

# Installation and run

1. Install docker.
2. Run `run_docker_test.sh` to create + start machines and run tests on them.
