#include "cxxopts.hpp"
#include "hermes.hpp"

#include <algorithm>
#include <unistd.h>

const std::string app_subsystem = "iallreduce_bench";

std::unique_ptr<char> get_pagesize_aligned_hostmem_buf(size_t size) {
    auto pagesize = sysconf(_SC_PAGE_SIZE);
    if (pagesize <= 0) {
        std::cerr << HERMES_LOG("alloc") << "This application depends on the initialized OOB communication context" << std::endl;
        hermes::generic_fatal_err_handler("alloc", EXIT_FAILURE);
    }
    return std::unique_ptr<char>(new (std::align_val_t(pagesize)) char[size]);
}

struct iallreduce_app : public hermes::app_ctx {
    iallreduce_app(int msg_size) : _buf(get_pagesize_aligned_hostmem_buf(msg_size)) {
        if (!hermes::oob_comm::is_initialized()) {
            std::cerr << HERMES_LOG(app_subsystem) << "This application depends on the initialized OOB communication context" << std::endl;
            hermes::generic_fatal_err_handler(app_subsystem, EXIT_FAILURE);
        }
    }

    void post_nb_comm(int conn_id, int flow_size) {
        HERMES_CHKERR(MPI_Iallreduce(MPI_IN_PLACE, _buf.get(), flow_size, MPI_CHAR, MPI_SUM, MPI_COMM_WORLD, &_req),
                      hermes::oob_comm::ret_success, hermes::oob_comm::mpi_err_handler, app_subsystem);
    }

    int wait_nb_comm() {
        MPI_Status status;
        HERMES_CHKERR(MPI_Wait(&_req, &status),
                      hermes::oob_comm::ret_success, hermes::oob_comm::mpi_err_handler, app_subsystem);
        return 0;
    }

    // TODO: Add IMB NBC benchmark copyright (BSD-3 license)
    void do_comp(double target_secs) {
        constexpr size_t arr_size = 10;
        static float a[arr_size][arr_size], x[arr_size], y[arr_size];
        static bool initialized = false;
        static int Nrep, target_reps;
        double t1, t2;
        int i, j, repeat;

        if (target_secs <= 0.) {
            return;
        }

        if (!initialized) {
            for (i = 0; i < arr_size; i++) {
                x[i] = y[i] = 0.;
                for (j = 0; j < arr_size; j++) {
                    a[i][j] = 1.;
                }
            }
            Nrep = (50000000 / (2 * arr_size * arr_size)) + 1;
            t1 = MPI_Wtime();
            for (repeat = 0; repeat < Nrep; repeat++) {
                for (i = 0; i < arr_size; i++) {
                    for (j = 0; j < arr_size; j++) {
                        x[i] = x[i] + a[i][j] * y[j];
                    }
                }
            }
            t2 = MPI_Wtime();

            auto MFlops = (Nrep * 2 * arr_size * arr_size) * 1.e-6 / (t2 - t1);
            Nrep = (int)(1. / (t2 - t1) * Nrep);
            target_reps = 0;
        }

        if (initialized) {
            target_reps = std::max(1, (int)(target_secs * Nrep));
            for (repeat = 0; repeat < target_reps; repeat++) {
                for (i = 0; i < arr_size; i++) {
                    for (j = 0; j < arr_size; j++) {
                        x[i] = x[i] + a[i][j] * y[j];
                    }
                }
            }
        } else {
            for (repeat = 0; repeat < target_reps; repeat++) {
                for (i = 0; i < arr_size; i++) {
                    for (j = 0; j < arr_size; j++) {
                        x[i] = x[i] + a[i][j] * y[j];
                    }
                }
            }
            initialized = true;
        }
    }

    size_t get_conn_number() const {
        return 1;
    }

    std::string get_name() const {
        return "iallreduce_app";
    }

    int get_proc_type() const {
        return 0;
    }

    std::string proc_type_to_str(int type) const {
        return "";
    }

    MPI_Request _req;
    std::unique_ptr<char> _buf;
};

const std::string invalid_string_opt_val = "invalid-value-0xdeadbeaf";

void opts_parse(int argc, char **argv, cxxopts::ParseResult &opts) {
    cxxopts::Options options("bmark", "Non-blocking collectives overlap benchmark");
    options.add_options()
        // Transport parameters
        ("S,msg_size", "Allreduce msg size", cxxopts::value<int>()->default_value("262144"))                                                                            //
        ("n,iters", "Number of benchmark iterations, N", cxxopts::value<std::size_t>()->default_value("10"))                                                            //
        ("w,warmup", "Number of warmup iterations, N", cxxopts::value<std::size_t>()->default_value("10"))                                                              //
        ("o,csv_path_prefix", "CSV output path prefix to dump *.pure_comm.csv and *.overlap.csv", cxxopts::value<std::string>()->default_value(invalid_string_opt_val)) //
        ("c,overlap", "Measure communication/computation overlap")                                                                                                      //
        ("v,verbose", "Print per-process statistics")                                                                                                                   //
        ("h,help", "Print usage");
    opts = options.parse(argc, argv);
    if (opts.count("help")) {
        std::cout << options.help() << std::endl;
        exit(0);
    }
}

double get_overlap_percentage(double pure_comm_t, double ovrlp_comm_t, double pure_comp_t) {
    return 100.0 * std::max(0.0, std::min(1.0, (pure_comm_t + pure_comp_t - ovrlp_comm_t) / std::min(pure_comm_t, pure_comp_t)));
}

std::unique_ptr<hermes::measurements_data> run_pure_comm(cxxopts::ParseResult &opts) {
    auto flow_size = opts["msg_size"].as<int>();
    iallreduce_app app(flow_size);

    hermes::measurements_data pure_comm_perf_data(app.get_conn_number(), opts["warmup"].as<std::size_t>(), opts["iters"].as<std::size_t>(), flow_size, 0.0);
    if (hermes::oob_comm::my_rank == 0) {
        std::cerr << HERMES_LOG(app_subsystem) << "Started pure comm time measurements..." << std::endl;
    }
    hermes::perf::eval_nb_loop(app, pure_comm_perf_data);
    std::vector<int> proc_types = app.get_all_participants_proc_types(0, MPI_COMM_WORLD);
    std::unique_ptr<hermes::measurements_data> global_pure_comm_perf_data(new hermes::measurements_data(pure_comm_perf_data, 0, MPI_COMM_WORLD));
    if (hermes::oob_comm::my_rank == 0) {
        if (opts.count("verbose")) {
            for (auto proc_id = 0; proc_id < global_pure_comm_perf_data->n_connections.size(); proc_id++) {
                hermes::perf::print_proc_summary(app, proc_types, *global_pure_comm_perf_data, proc_id, "pure_comm");
            }
        }
        hermes::perf::print_overall_summary(app, *global_pure_comm_perf_data, "pure_comm");
        if (opts["csv_path_prefix"].as<std::string>() != invalid_string_opt_val) {
            hermes::perf::write_csv(app, proc_types, *global_pure_comm_perf_data, opts["csv_path_prefix"].as<std::string>() + ".pure_comm.csv");
        }
    }
    return global_pure_comm_perf_data;
}

void run_overlap(cxxopts::ParseResult &opts, const std::unique_ptr<hermes::measurements_data> &global_pure_comm_perf_data) {

    // Get estimation of target compute time needed to overlap communication
    // The assumption here is that the amount of compute (e.g., training batch size) is expected to be the same across nodes
    double target_comp_time = -1.0;
    if (hermes::oob_comm::my_rank == 0) {
        for (auto proc_id = 0; proc_id < global_pure_comm_perf_data->n_connections.size(); proc_id++) {
            auto perf_iters_start_id = global_pure_comm_perf_data->get_item_storage_idx(proc_id, 0, global_pure_comm_perf_data->warmup_iters);
            auto perf_iters_end_id = global_pure_comm_perf_data->get_item_storage_idx(proc_id, 0, global_pure_comm_perf_data->total_iters);
            auto proc_avg_pure_comm_time = hermes::perf::get_avg<double>(global_pure_comm_perf_data->comm_times, perf_iters_start_id, perf_iters_end_id);
            if (proc_avg_pure_comm_time > target_comp_time) {
                target_comp_time = proc_avg_pure_comm_time;
            }
        }
    }
    HERMES_CHKERR(MPI_Bcast(&target_comp_time, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD), hermes::oob_comm::ret_success, hermes::oob_comm::mpi_err_handler, app_subsystem);

    // Get measurements when computation is overlapped with communication
    auto flow_size = opts["msg_size"].as<int>();
    iallreduce_app app(flow_size);
    hermes::measurements_data comp_perf_data(app.get_conn_number(), opts["warmup"].as<std::size_t>(), opts["iters"].as<std::size_t>(), flow_size, target_comp_time);
    if (hermes::oob_comm::my_rank == 0) {
        std::cerr << HERMES_LOG(app_subsystem) << "Started time measurements with compute..." << std::endl;
    }
    hermes::perf::eval_nb_loop(app, comp_perf_data);

    // Get performance summary
    std::vector<int> proc_types = app.get_all_participants_proc_types(0, MPI_COMM_WORLD);
    hermes::measurements_data global_comp_perf_data(comp_perf_data, 0, MPI_COMM_WORLD);
    if (hermes::oob_comm::my_rank == 0) {
        std::vector<double> overlap(global_comp_perf_data.n_connections.size(), 0);
        for (auto proc_id = 0; proc_id < global_comp_perf_data.n_connections.size(); proc_id++) {
            auto perf_iters_start_id = global_comp_perf_data.get_item_storage_idx(proc_id, 0, global_comp_perf_data.warmup_iters);
            auto perf_iters_end_id = global_comp_perf_data.get_item_storage_idx(proc_id, 0, global_comp_perf_data.total_iters);
            auto avg_pure_comm_time = hermes::perf::get_avg<double>(global_pure_comm_perf_data->comm_times, perf_iters_start_id, perf_iters_end_id);
            auto avg_ovrlp_comm_time = hermes::perf::get_avg<double>(global_comp_perf_data.comm_times, perf_iters_start_id, perf_iters_end_id);
            auto avg_pure_comp_time = hermes::perf::get_avg<double>(global_comp_perf_data.comp_times, perf_iters_start_id, perf_iters_end_id);
            overlap[proc_id] = get_overlap_percentage(avg_pure_comm_time, avg_ovrlp_comm_time, avg_pure_comp_time);
            if (opts.count("verbose")) {
                hermes::perf::print_proc_summary(app, proc_types, global_comp_perf_data, proc_id, "overlap=" + std::to_string(overlap[proc_id]) + "\%");
            }
        }
        auto avg_overlap = hermes::perf::get_avg<double>(overlap, 0, overlap.size());
        hermes::perf::print_overall_summary(app, global_comp_perf_data, "overlap=" + std::to_string(avg_overlap) + "\%");
        if (opts["csv_path_prefix"].as<std::string>() != invalid_string_opt_val) {
            hermes::perf::write_csv(app, proc_types, global_comp_perf_data, opts["csv_path_prefix"].as<std::string>() + ".overlap.csv");
        }
    }
}

int main(int argc, char **argv) {
    cxxopts::ParseResult opts;
    opts_parse(argc, argv, opts);
    hermes::init(argc, argv, hermes::default_config);
    {
        auto global_comm_perf_data = run_pure_comm(opts);
        if (opts.count("overlap")) {
            run_overlap(opts, global_comm_perf_data);
        }
    }
    hermes::finalize();
    return 0;
}