#include "cxxopts.hpp"
#include "hermes.hpp"
#include <algorithm>
#include <iomanip>
#include <mpi.h>
#include <unistd.h>

static double start_time;
static void init_time() {
    start_time = hermes::oob_comm::mpi_wtime();
}
static inline double get_time() {
    return hermes::oob_comm::mpi_wtime() - start_time;
}

/*
Calculcate the L1 error given the measured timestamps.
*/
static double clock_difference;
double _calculate_l1_error(double current_rank_time) {
    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    double average;
    auto global_time_of_this_node = current_rank_time + clock_difference;
    HERMES_CHKERR(MPI_Allreduce(&global_time_of_this_node, &average, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD), hermes::oob_comm::ret_success, hermes::oob_comm::mpi_err_handler, hermes::perf::subsystem);

    average /= size;

    // Calculate the L1 error
    double l1_error = 0;
    auto this_node_time = std::abs(global_time_of_this_node - average);
    HERMES_CHKERR(MPI_Allreduce(&this_node_time, &l1_error, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD), hermes::oob_comm::ret_success, hermes::oob_comm::mpi_err_handler, hermes::perf::subsystem);

    l1_error /= size;

    return l1_error;
}

/*
    Run some MPI primitives and estimate the L1 error.
    Returned error is in seconds, and is calculated like this:
    mean(abs(barrier_exit_times[i] - average_barrier_exit_time) where i is in [nodes])

    This uses RTT to measure it.
    Therefore, it may not be reliable if RTT is not reliable.

    Returns: it returns a triple
             (l1 error for this barrier, l1 error for MPI_Barrier, l1 error for no barrier)
             All processes get the same result.
    */
std::tuple<double, double, double> measure_l1_error() {
    double now;

    hermes::time::global_clock_barrier my_synchronization_object(MPI_COMM_WORLD, 1000, 1000);
    clock_difference = my_synchronization_object.get_global_clock_diff();
    std::cout << "Clock difference: " << clock_difference << std::endl;

    // Custom Barrier
    int num_tries = 1000;
    double l1_error_custom_barrier = 0;
    // Warmup
    for (int i = 0; i < num_tries; i++) {
        my_synchronization_object.sync();
        now = get_time();
        l1_error_custom_barrier += _calculate_l1_error(now);
    }
    l1_error_custom_barrier = 0;
    for (int i = 0; i < num_tries; i++) {
        my_synchronization_object.sync();
        now = get_time();
        l1_error_custom_barrier += _calculate_l1_error(now);
    }
    l1_error_custom_barrier /= num_tries;

    // MPI Barrier
    double l1_error_mpi_barrier = 0;
    // Warmup
    for (int i = 0; i < num_tries; i++) {
        HERMES_CHKERR(MPI_Barrier(MPI_COMM_WORLD), hermes::oob_comm::ret_success, hermes::oob_comm::mpi_err_handler, hermes::perf::subsystem);
        now = get_time();
        l1_error_mpi_barrier += _calculate_l1_error(now);
    }
    l1_error_mpi_barrier = 0;
    for (int i = 0; i < num_tries; i++) {
        HERMES_CHKERR(MPI_Barrier(MPI_COMM_WORLD), hermes::oob_comm::ret_success, hermes::oob_comm::mpi_err_handler, hermes::perf::subsystem);
        now = get_time();
        l1_error_mpi_barrier += _calculate_l1_error(now);
    }
    l1_error_mpi_barrier /= num_tries;

    // No Barrier
    double l1_error_no_barrier = 0;
    // Warmup
    for (int i = 0; i < num_tries; i++) {
        now = get_time();
        l1_error_no_barrier += _calculate_l1_error(now);
    }
    l1_error_no_barrier = 0;
    for (int i = 0; i < num_tries; i++) {
        now = get_time();
        l1_error_no_barrier += _calculate_l1_error(now);
    }
    l1_error_no_barrier /= num_tries;

    return std::make_tuple(l1_error_custom_barrier, l1_error_mpi_barrier, l1_error_no_barrier);
}

const std::string app_subsystem = "barrier_bench";

int main(int argc, char **argv) {
    hermes::init(argc, argv, hermes::default_config);
    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    init_time();
    auto rtt = measure_l1_error();
    if (rank == 0) {
        std::cout << "CustomBarrier L1 error: " << std::setprecision(15) << std::scientific << std::get<0>(rtt) << std::endl;
        std::cout << "MPI_Barrier L1 error  : " << std::setprecision(15) << std::scientific << std::get<1>(rtt) << std::endl;
        std::cout << "NoBarrier L1 error    : " << std::setprecision(15) << std::scientific << std::get<2>(rtt) << std::endl;
    }
    hermes::finalize();

    return 0;
}
