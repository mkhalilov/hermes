#include <fstream>
#include <iostream>
#include <limits>
#include <mpi.h>
#include <numeric>
#include <stdexcept>
#include <string>
#include <vector>
// I am adding this because my compiler cannot find function declarations like min_element, std::sort
#include <algorithm>

// TODOs:
//
// Infrastructure:
// C++:
// - modern C++:
//     - decorators where needed
//     - check what sugar/idioms from the C++11/17/23 we can use in the code
// - add core tests
// - add jupyter notebooks that can parse per-proxy-app CSVs
//
// Core:
// - Time measurements
//      - backport distributed clock synchronization from the netgauge, mpiprof and Hoefler et.al. "Accurately Measuring Collective Operations at Massive Scale".
// - hermes::memory
//      - add memory allocator abstract class that can support:
//          - memory alignment
//          - various memory types: host, CUDA, ROCm, etc.
//          - function to touch memory buffer
// - add hermes::measurements_data(..) constructor with statistical comm_size generation, e.g., comm_size argument can be a lambda
//
// SHARP-related functions:
// - support for communication/computation overlap-related measurements
//      - add hermes::measurements_data::get_avg_overlap(..)
//      - add MPI collectives proxy app
// - support for multi-tenancy:
//      - multiple hermes::app instances running on the different cluster nodes:
//          - initial synchronization of apps during MPI_Init()?
//          - per-app logging?
//      - multi-communicator proxy app
// - support for hermes:app resource profiling
//      - CPU utilization
//      - memory consumption

namespace hermes {

struct config {
    enum ts_backend {
        TS_BACKEND_MPI_WTIME = 0,
        // enumerate other ts backends here
    } ts;
    enum sync_backend {
        SYNC_BACKEND_MPI_BARRIER = 0,
        // enumerate other sync backends here
    } sync;
    bool needs_oob;
};

#define HERMES_LOG(subsystem) \
    (std::string("[" + std::to_string(hermes::oob_comm::my_rank) + ":" + subsystem + ":" + std::string(__func__) + ":" + std::to_string(__LINE__) + "] "))
#define HERMES_CHKERR(fn, expected_ret, fatal_err_handler, subsystem) \
    {                                                                 \
        auto ret = fn;                                                \
        if (ret != expected_ret) {                                    \
            fatal_err_handler(subsystem, ret);                        \
        }                                                             \
    }
void generic_fatal_err_handler(const std::string &subsystem, int ret);

namespace oob_comm {

const std::string subsystem = "oob_comm";
const int ret_success = MPI_SUCCESS;
int my_rank = -1;
int world_size = -1;

void mpi_err_handler(const std::string &subsystem, int ret) {
    std::cerr << HERMES_LOG(subsystem) << "MPI error" << ret << " occurred" << std::endl;
    char err_string[MPI_MAX_ERROR_STRING];
    int err_string_len;
    MPI_Error_string(ret, err_string, &err_string_len);
    std::cerr << HERMES_LOG(subsystem) << err_string << std::endl;
    MPI_Abort(MPI_COMM_WORLD, ret);
}

bool is_initialized() {
    int is_mpi_initialized;
    HERMES_CHKERR(MPI_Initialized(&is_mpi_initialized),
                  hermes::oob_comm::ret_success, hermes::oob_comm::mpi_err_handler, subsystem);
    return is_mpi_initialized;
}

void mpi_init(int argc, char **argv) {
    HERMES_CHKERR(MPI_Init(&argc, &argv), ret_success, mpi_err_handler, subsystem);
    HERMES_CHKERR(MPI_Comm_rank(MPI_COMM_WORLD, &my_rank), ret_success, mpi_err_handler, subsystem);
    HERMES_CHKERR(MPI_Comm_size(MPI_COMM_WORLD, &world_size), ret_success, mpi_err_handler, subsystem);
}

void mpi_finalize() {
    HERMES_CHKERR(MPI_Finalize(), ret_success, mpi_err_handler, subsystem);
}

inline void mpi_barrier(MPI_Comm comm) { HERMES_CHKERR(MPI_Barrier(comm), ret_success, mpi_err_handler, subsystem); }
inline void mpi_world_barrier() { HERMES_CHKERR(MPI_Barrier(MPI_COMM_WORLD), ret_success, mpi_err_handler, subsystem); }
inline double mpi_wtime() { return MPI_Wtime(); }

} // namespace oob_comm

void generic_fatal_err_handler(const std::string &subsystem, int ret) {
    std::cerr << HERMES_LOG(subsystem) << "Generic error handler invoked" << std::endl;
    if (oob_comm::is_initialized()) {
        MPI_Abort(MPI_COMM_WORLD, ret);
    }
    exit(ret);
}

namespace time {

const std::string subsytem = "time";
void (*sync_cb)() = nullptr;
double (*get_ts_cb)() = nullptr;

void sync_init(int backend_id) {
    switch (backend_id) {
    case (config::SYNC_BACKEND_MPI_BARRIER):
        sync_cb = oob_comm::mpi_world_barrier;
        std::cerr << HERMES_LOG(subsytem) << "Barrier synchronization backend is initialized with MPI_Barrier(MPI_COMM_WORLD)" << std::endl;
        break;
    default:
        sync_cb = oob_comm::mpi_world_barrier;
        std::cerr << HERMES_LOG(subsytem) << "Unknown synchronization backend requested. Fallback to MPI_Barrier(MPI_COMM_WORLD) as default." << std::endl;
    }
}

void ts_init(int backend_id) {
    switch (backend_id) {
    case (config::TS_BACKEND_MPI_WTIME):
        get_ts_cb = oob_comm::mpi_wtime;
        std::cerr << HERMES_LOG(subsytem) << "Timestamp backend is initialized with MPI_Wtime()" << std::endl;
        break;
    default:
        get_ts_cb = oob_comm::mpi_wtime;
        std::cerr << HERMES_LOG(subsytem) << "Unknown timestamp backend requested. Fallback to MPI_Wtime() as default." << std::endl;
    }
}

inline void sync() { sync_cb(); }
inline double get_ts() { return get_ts_cb(); }
inline double get_ts_diff(double ts_start, double ts_end) { return ts_end - ts_start; }

class global_clock_barrier {
public:
    /*
    This class implements a barrier that also indicates whether the rank that called it exits at the same time as others or later.
    This is achieved by synchronizing the clocks behind the scene, and then using the high precision time (MPI_Wtime) to determine when to exit the barrier.
    As a result, ideally all processes will exit the barrier at the same time independent from the communication delays between them.
    If they do not, the process which returned late will return 1, and the others will return 0.

    @param comm: MPI communicator
    @param n_rtt_samples: is the number of times to optimize for RTT (default 100), defined in https://htor.inf.ethz.ch/publications/img/hoefler-pmeo08.pdf.
    @param n_bcast_time_samples: we run number of MPI_Barrier calls to accumulate time which defines the time every process waits during sync call.
                                 For example, if n_bcast_time_samples = 10 that means every thread will wait int the sync call until the slowest of the nodes can perform 10 MPI_Barrier calls
    Example:
        global_clock_barrier barrier(MPI_COMM_WORLD, global_clock_barrier::default_n_rtt_samples, global_clock_barrier::default_n_bcast_time_samples);
    */
    global_clock_barrier(MPI_Comm comm, size_t n_rtt_samples, size_t n_bcast_time_samples)
        : comm(comm),
          n_bcast_time_samples(n_bcast_time_samples),
          n_rtt_samples(n_rtt_samples) {

        if (!hermes::oob_comm::is_initialized()) {
            std::cerr << HERMES_LOG(_log_subsystem) << "The constructor failed because MPI was not initialized." << std::endl;
            generic_fatal_err_handler(_log_subsystem, EXIT_FAILURE);
        }

        start_time_reference = hermes::oob_comm::mpi_wtime();
        HERMES_CHKERR(MPI_Comm_rank(comm, &my_rank), hermes::oob_comm::ret_success, hermes::oob_comm::mpi_err_handler, _log_subsystem);
        HERMES_CHKERR(MPI_Comm_size(comm, &comm_size), hermes::oob_comm::ret_success, hermes::oob_comm::mpi_err_handler, _log_subsystem);

        global_clock_difference = get_global_clock_diff();

        bcast_time = estimate_bcast_time();
    }

    int sync() {
        double target_exit_time;
        if (my_rank == 0) {
            target_exit_time = get_time() + bcast_time;
        }
        HERMES_CHKERR(MPI_Bcast(&target_exit_time, 1, MPI_DOUBLE, 0, comm), hermes::oob_comm::ret_success, hermes::oob_comm::mpi_err_handler, _log_subsystem);

        // All nodes corrects the timestamps
        // Assume clock is 100 in node 0, but it's 150 in node 1
        // Assume we wait until clock is 150 in node 0
        // In this case, we need to wait until 200 in node 1
        // So, we need to wait 50 cycles (but difference was -50), so we subtract
        target_exit_time -= global_clock_difference;

        if (get_time() > target_exit_time) {
            return 1;
        }

        while (get_time() < target_exit_time) {
            // Wait until the time comes
        }
        return 0;
    }

    /*
    This function measures the slowest node in the network.
    Simply, run broadcast 10 or 100 times, and get the worst time from all nodes.
    The worst time is the point where we need to synchronize every node.

    @return: every process gets the maximum time to wait after sequence of MPI_Bcast calls completes
    */

    double estimate_bcast_time() {
        double dummy_bcast_message = 0xDEADBEAF;
        auto start_time = get_time();
        for (auto i = 0; i < n_bcast_time_samples; i++) {
            HERMES_CHKERR(MPI_Bcast(&dummy_bcast_message, 1, MPI_DOUBLE, 0, comm), hermes::oob_comm::ret_success, hermes::oob_comm::mpi_err_handler, _log_subsystem);
        }
        auto end_time = get_time();
        auto total_time = end_time - start_time;

        double max_bcast_time;
        HERMES_CHKERR(MPI_Reduce(&total_time, &max_bcast_time, 1, MPI_DOUBLE, MPI_MAX, 0, comm), hermes::oob_comm::ret_success, hermes::oob_comm::mpi_err_handler, _log_subsystem);

        return max_bcast_time;
    }

    /*
    This function calculates the clock differences between nodes using RTT (round trip time).
    It uses the algorithm defined here https://htor.inf.ethz.ch/publications/img/hoefler-pmeo08.pdf.
    It uses the `n_rtt_samples` parameter to be the consequentive iterations without a better RTT (N in the paper).

    @return: every caller will get it's clock difference relative to the root node.
             positive if root is ahead, negative if the target rank is ahead.
             so, if you have local time local_time, you can subtract the difference to get the global_time = local_time - difference
    */

    double get_global_clock_diff() {
        enum signal_type {
            continue_rtt_sampling = 0,
            stop_rtt_sampling = 1
        } signal;

        if (my_rank == 0) {
            for (auto target_rank = 1; target_rank < comm_size; target_rank++) {
                int n = 0;
                auto estimated_clock_diff = std::numeric_limits<double>::max();
                auto min_rtt = std::numeric_limits<double>::max();
                while (n < n_rtt_samples) {
                    signal = continue_rtt_sampling;
                    HERMES_CHKERR(MPI_Send(&signal, 1, MPI_INT, target_rank, 0, comm), hermes::oob_comm::ret_success, hermes::oob_comm::mpi_err_handler, _log_subsystem);

                    double rtt_ping = 0.0;
                    auto start_time = get_time();
                    HERMES_CHKERR(MPI_Send(&rtt_ping, 1, MPI_DOUBLE, target_rank, 0, comm), hermes::oob_comm::ret_success, hermes::oob_comm::mpi_err_handler, _log_subsystem);

                    double remote_node_local_time = 0.0;
                    HERMES_CHKERR(MPI_Recv(&remote_node_local_time, 1, MPI_DOUBLE, target_rank, 0, comm, MPI_STATUS_IGNORE), hermes::oob_comm::ret_success, hermes::oob_comm::mpi_err_handler, _log_subsystem);

                    auto end_time = get_time();
                    auto cur_rtt = end_time - start_time;

                    // Take the best
                    if (cur_rtt < min_rtt) {
                        min_rtt = cur_rtt;
                        auto mid_time = (end_time + start_time) / 2.0;
                        estimated_clock_diff = mid_time - remote_node_local_time;

                        // We need to set the number to zero because the paper defines n, as the consequent number of iterations without a better RTT
                        n = 0;
                    }

                    n++;
                }
                signal = stop_rtt_sampling;
                HERMES_CHKERR(MPI_Send(&signal, 1, MPI_INT, target_rank, 0, comm), hermes::oob_comm::ret_success, hermes::oob_comm::mpi_err_handler, _log_subsystem);

                HERMES_CHKERR(MPI_Send(&estimated_clock_diff, 1, MPI_DOUBLE, target_rank, 0, comm), hermes::oob_comm::ret_success, hermes::oob_comm::mpi_err_handler, _log_subsystem);
            }
            return 0;
        } else {
            while (true) {
                HERMES_CHKERR(MPI_Recv(&signal, 1, MPI_INT, 0, 0, comm, MPI_STATUS_IGNORE), hermes::oob_comm::ret_success, hermes::oob_comm::mpi_err_handler, _log_subsystem);
                if (signal == stop_rtt_sampling) {
                    double estimated_clock_diff;
                    HERMES_CHKERR(MPI_Recv(&estimated_clock_diff, 1, MPI_DOUBLE, 0, 0, comm, MPI_STATUS_IGNORE), hermes::oob_comm::ret_success, hermes::oob_comm::mpi_err_handler, _log_subsystem);
                    return estimated_clock_diff;
                }

                double rtt_ping = 0;
                HERMES_CHKERR(MPI_Recv(&rtt_ping, 1, MPI_DOUBLE, 0, 0, comm, MPI_STATUS_IGNORE), hermes::oob_comm::ret_success, hermes::oob_comm::mpi_err_handler, _log_subsystem);
                auto local_time = get_time();
                HERMES_CHKERR(MPI_Send(&local_time, 1, MPI_DOUBLE, 0, 0, comm), hermes::oob_comm::ret_success, hermes::oob_comm::mpi_err_handler, _log_subsystem);
            }
        }
    }

    static const size_t default_n_rtt_samples = 100;
    static const size_t default_n_bcast_time_samples = 10;

private:
    const std::string _log_subsystem = "global_clock_barrier";
    // We store the initial time of the object here so that the operations on the object do not overflow
    double start_time_reference;

    double get_time() {
        return hermes::oob_comm::mpi_wtime() - start_time_reference;
    }

    // Number of times to optimize for RTT
    size_t n_rtt_samples;
    size_t n_bcast_time_samples;
    MPI_Comm comm;

    double bcast_time;

    double global_clock_difference;

    int my_rank;
    int comm_size;
};

} // namespace time

struct measurements_data {
    constexpr static double ts_init_value = std::numeric_limits<double>::min();
    // eval_nb_loop loop input
    std::vector<int> n_connections;
    size_t warmup_iters;
    size_t perf_iters;
    size_t total_iters;
    std::vector<int> comm_sizes;
    std::vector<double> target_comp_times;
    // eval_nb_loop loop output
    std::vector<double> comm_times; // communication time (including compute)
    std::vector<double> comp_times; // computation time

    inline size_t get_size_bytes() const {
        return (sizeof(double) * 3 + sizeof(int)) * comm_sizes.size();
    }

    inline size_t get_item_storage_idx(int proc_id, size_t conn_id, size_t iter) const {
        return std::reduce(n_connections.begin(), n_connections.begin() + proc_id) * total_iters + iter;
    }

private:
    const std::string _log_subsystem = "measurements_data";

public:
    measurements_data(int n_conns, size_t wrmp_iters, size_t prf_iters,
                      int comm_size, double comp_target_time)
        : n_connections(1, n_conns),
          warmup_iters(wrmp_iters),
          perf_iters(prf_iters),
          total_iters(warmup_iters + perf_iters),
          comm_sizes(n_connections[0] * total_iters, comm_size),
          target_comp_times(n_connections[0] * total_iters, comp_target_time),
          comm_times(n_connections[0] * total_iters, ts_init_value),
          comp_times(n_connections[0] * total_iters, ts_init_value) {
        if (!(prf_iters > 0)) {
            std::cerr << HERMES_LOG(_log_subsystem) << "number of performance iterations should be > 0" << std::endl;
            generic_fatal_err_handler(_log_subsystem, EXIT_FAILURE);
        }
        if (comm_size < 0) {
            std::cerr << HERMES_LOG(_log_subsystem) << "communication size should be >= 0" << std::endl;
            generic_fatal_err_handler(_log_subsystem, EXIT_FAILURE);
        }
        if (comp_target_time < 0.0) {
            std::cerr << HERMES_LOG(_log_subsystem) << "compute target time should be >= 0s" << std::endl;
            generic_fatal_err_handler(_log_subsystem, EXIT_FAILURE);
        }
        if (comp_target_time > 0.0 and n_connections[0] > 1) {
            std::cerr << HERMES_LOG(_log_subsystem) << "measurement of compute is not supported with more than one communication context per process" << std::endl;
            generic_fatal_err_handler(_log_subsystem, EXIT_FAILURE);
        } else {
            std::cerr << HERMES_LOG(_log_subsystem) << "sizeof(measurements_data)=" << static_cast<double>(get_size_bytes()) / 1e6 << "MB" << std::endl;
        }
    }

    measurements_data(const measurements_data &local_measurements, int root, MPI_Comm comm)
        : warmup_iters(local_measurements.warmup_iters),
          perf_iters(local_measurements.perf_iters),
          total_iters(local_measurements.total_iters) {
        if (!hermes::oob_comm::is_initialized()) {
            std::cerr << HERMES_LOG(_log_subsystem) << "This constructor depends on the initialized OOB communication context" << std::endl;
            generic_fatal_err_handler(_log_subsystem, EXIT_FAILURE);
        }
        int comm_size, my_rank;
        HERMES_CHKERR(MPI_Comm_size(comm, &comm_size), oob_comm::ret_success, oob_comm::mpi_err_handler, _log_subsystem);
        HERMES_CHKERR(MPI_Comm_rank(comm, &my_rank), oob_comm::ret_success, oob_comm::mpi_err_handler, _log_subsystem);
        if (local_measurements.n_connections.size() != 1) {
            std::cerr << HERMES_LOG(_log_subsystem) << "This measurements object can be constructed only from a single-app measurement results" << std::endl;
            generic_fatal_err_handler(_log_subsystem, EXIT_FAILURE);
        }
        if (my_rank == root) {
            n_connections.resize(comm_size);
        }
        HERMES_CHKERR(MPI_Gather(&local_measurements.n_connections[0], 1, MPI_INT, n_connections.data(), 1, MPI_INT, root, comm),
                      oob_comm::ret_success, oob_comm::mpi_err_handler, _log_subsystem);
        std::vector<int> recvcounts(comm_size);
        std::vector<int> displs(comm_size);
        if (my_rank == root) {
            int cur_displ = 0;
            for (auto proc = 0; proc < comm_size; proc++) {
                if (n_connections[proc] <= 0) {
                    std::cerr << HERMES_LOG(_log_subsystem) << "Abort. Number of connections received from proc="
                              << proc << " is " << n_connections[proc] << ", but should be >0" << std::endl;
                    return;
                }
                recvcounts[proc] = total_iters * n_connections[proc];
                displs[proc] = cur_displ;
                cur_displ += recvcounts[proc];
            }
            auto total_conns = std::reduce(n_connections.begin(), n_connections.end());
            comm_sizes.resize(total_conns * total_iters);
            target_comp_times.resize(total_conns * total_iters);
            comm_times.resize(total_conns * total_iters);
            comp_times.resize(total_conns * total_iters);
            std::cerr << HERMES_LOG(_log_subsystem) << "sizeof(measurements_data)=" << static_cast<double>(get_size_bytes()) / 1e6 << "MB" << std::endl;
        }
        HERMES_CHKERR(MPI_Gatherv(local_measurements.comm_sizes.data(), local_measurements.n_connections[0] * total_iters, MPI_INT,
                                  comm_sizes.data(), recvcounts.data(), displs.data(), MPI_INT, root, comm),
                      oob_comm::ret_success, oob_comm::mpi_err_handler, _log_subsystem);
        HERMES_CHKERR(MPI_Gatherv(local_measurements.target_comp_times.data(), local_measurements.n_connections[0] * total_iters, MPI_DOUBLE,
                                  target_comp_times.data(), recvcounts.data(), displs.data(), MPI_DOUBLE, root, comm),
                      oob_comm::ret_success, oob_comm::mpi_err_handler, _log_subsystem);
        HERMES_CHKERR(MPI_Gatherv(local_measurements.comm_times.data(), local_measurements.n_connections[0] * total_iters, MPI_DOUBLE,
                                  comm_times.data(), recvcounts.data(), displs.data(), MPI_DOUBLE, root, comm),
                      oob_comm::ret_success, oob_comm::mpi_err_handler, _log_subsystem);
        HERMES_CHKERR(MPI_Gatherv(local_measurements.comp_times.data(), local_measurements.n_connections[0] * total_iters, MPI_DOUBLE,
                                  comp_times.data(), recvcounts.data(), displs.data(), MPI_DOUBLE, root, comm),
                      oob_comm::ret_success, oob_comm::mpi_err_handler, _log_subsystem);
    }
};

struct app_ctx {
    virtual ~app_ctx() {};
    virtual void post_nb_comm(int conn_id, int comm_size) = 0;
    virtual int wait_nb_comm() = 0;
    virtual void do_comp(double time) = 0;
    virtual size_t get_conn_number() const = 0;
    virtual std::string get_name() const = 0;
    virtual int get_proc_type() const = 0;
    virtual std::string proc_type_to_str(int type) const = 0;
    std::vector<int> get_all_participants_proc_types(int root, MPI_Comm comm) {
        int comm_size, my_rank;
        HERMES_CHKERR(MPI_Comm_size(comm, &comm_size), oob_comm::ret_success, oob_comm::mpi_err_handler, "app_ctx");
        HERMES_CHKERR(MPI_Comm_rank(comm, &my_rank), oob_comm::ret_success, oob_comm::mpi_err_handler, "app_ctx");
        std::vector<int> proc_types(comm_size, -1);
        proc_types[my_rank] = get_proc_type();
        HERMES_CHKERR(MPI_Gather(my_rank == root ? MPI_IN_PLACE : &proc_types[my_rank], 1, MPI_INT, proc_types.data(), 1, MPI_INT, root, comm),
                      oob_comm::ret_success, oob_comm::mpi_err_handler, "app_ctx");
        return proc_types;
    }
};

namespace perf {

const std::string subsystem = "perf";

void eval_nb_loop(app_ctx &app, measurements_data &measurements) {
    if (measurements.n_connections.size() != 1) {
        std::cerr << HERMES_LOG(subsystem) << "measurements.n_connections.size() != 1" << std::endl;
        generic_fatal_err_handler(subsystem, EXIT_FAILURE);
    }
    std::vector<double> ts_comm_start(app.get_conn_number(), measurements_data::ts_init_value);
    std::vector<double> ts_comm_end(app.get_conn_number(), measurements_data::ts_init_value);
    for (auto cur_iter = 0; cur_iter < measurements.total_iters; cur_iter++) {
        auto remaining_endpoints = app.get_conn_number();
        time::sync();
        // Initiate communication within all active communication context
        for (auto conn_id = 0; conn_id < app.get_conn_number(); conn_id++) {
            ts_comm_start[conn_id] = time::get_ts();
            app.post_nb_comm(conn_id, measurements.comm_sizes[measurements.get_item_storage_idx(0, conn_id, cur_iter)]);
        }
        // TODO: check how this will map to the xCCL semantics when compute is done on the GPU
        auto ts_comp_start = time::get_ts();
        app.do_comp(measurements.target_comp_times[measurements.get_item_storage_idx(0, 0, cur_iter)]);
        auto ts_comp_end = time::get_ts();
        // With the perfect overlap the while loop below should should take almost no time.
        while (remaining_endpoints) {
            auto conn_id = app.wait_nb_comm();
            ts_comm_end[conn_id] = time::get_ts();
            measurements.comm_times[measurements.get_item_storage_idx(0, conn_id, cur_iter)] =
                time::get_ts_diff(ts_comm_start[conn_id], ts_comm_end[conn_id]);
            remaining_endpoints--;
        }
        measurements.comp_times[measurements.get_item_storage_idx(0, 0, cur_iter)] = time::get_ts_diff(ts_comp_start, ts_comp_end);
    }
}

static inline void validate_indices(size_t start_id, size_t end_id) {
    if (start_id >= end_id) {
        std::cerr << HERMES_LOG(subsystem) << "Error: start_id should be larger than end_id, start_id=" << start_id << " end_id=" << end_id << std::endl;
        generic_fatal_err_handler(subsystem, EXIT_FAILURE);
    }
}

template <typename T>
T get_avg(const std::vector<T> &v, size_t start_id, size_t end_id) {
    validate_indices(start_id, end_id);
    // perform accumulation in double to avoid overflow
    double acc = std::accumulate(v.begin() + start_id, v.begin() + end_id, 0., std::plus<double>());
    // then downcast it back to the T
    return static_cast<T>(acc / (end_id - start_id));
}

template <typename T>
T get_min(const std::vector<T> &v, size_t start_id, size_t end_id) {
    validate_indices(start_id, end_id);
    return *std::min_element(v.begin() + start_id, v.begin() + end_id);
}

template <typename T>
T get_max(const std::vector<T> &v, size_t start_id, size_t end_id) {
    validate_indices(start_id, end_id);
    return *std::max_element(v.begin() + start_id, v.begin() + end_id);
}

template <typename T>
T get_p99(const std::vector<T> &v, size_t start_id, size_t end_id) {
    validate_indices(start_id, end_id);
    if ((end_id - start_id) < 100) {
        return std::numeric_limits<T>::min();
    }
    std::vector<T> tmp(v.begin() + start_id, v.begin() + end_id);
    std::sort(tmp.begin(), tmp.end());
    auto p99_elem_it = tmp.begin() + tmp.size() / 100 * 99;
    return *p99_elem_it;
}

void print_overall_summary(const app_ctx &app, const measurements_data &measurements, const std::string &usr_experiment_info) {
    auto total_conns = std::reduce(measurements.n_connections.begin(), measurements.n_connections.end());
    std::vector<int> comm_size_avgs(total_conns, 0);
    std::vector<double> comm_t_avgs(total_conns, 0.0);
    std::vector<double> comm_t_mins(total_conns, 0.0);
    std::vector<double> comm_t_maxs(total_conns, 0.0);
    std::vector<double> comm_t_p99s(total_conns, 0.0);
    std::vector<double> comp_t_avgs(total_conns, 0.0);
    std::vector<double> comp_t_mins(total_conns, 0.0);
    std::vector<double> comp_t_maxs(total_conns, 0.0);
    std::vector<double> comp_t_p99s(total_conns, 0.0);
    size_t gconn_id = 0;
    for (auto proc_id = 0; proc_id < measurements.n_connections.size(); proc_id++) {
        for (auto lconn_id = 0; lconn_id < measurements.n_connections[proc_id]; lconn_id++, gconn_id++) {
            auto perf_iters_start_id = measurements.get_item_storage_idx(proc_id, lconn_id, measurements.warmup_iters);
            auto perf_iters_end_id = measurements.get_item_storage_idx(proc_id, lconn_id, measurements.total_iters);
            comm_size_avgs[gconn_id] = get_avg<int>(measurements.comm_sizes, perf_iters_start_id, perf_iters_end_id);
            comm_t_avgs[gconn_id] = get_avg<double>(measurements.comm_times, perf_iters_start_id, perf_iters_end_id);
            comm_t_mins[gconn_id] = get_min<double>(measurements.comm_times, perf_iters_start_id, perf_iters_end_id);
            comm_t_maxs[gconn_id] = get_max<double>(measurements.comm_times, perf_iters_start_id, perf_iters_end_id);
            comm_t_p99s[gconn_id] = get_p99<double>(measurements.comm_times, perf_iters_start_id, perf_iters_end_id);
            comp_t_avgs[gconn_id] = get_avg<double>(measurements.comp_times, perf_iters_start_id, perf_iters_end_id);
            comp_t_mins[gconn_id] = get_min<double>(measurements.comp_times, perf_iters_start_id, perf_iters_end_id);
            comp_t_maxs[gconn_id] = get_max<double>(measurements.comp_times, perf_iters_start_id, perf_iters_end_id);
            comp_t_p99s[gconn_id] = get_p99<double>(measurements.comp_times, perf_iters_start_id, perf_iters_end_id);
        }
    }
    auto comm_size_avg = get_avg<int>(comm_size_avgs, 0, total_conns);
    auto comm_t_avg = get_avg<double>(comm_t_avgs, 0, total_conns);
    auto comm_t_min = get_min<double>(comm_t_mins, 0, total_conns);
    auto comm_t_max = get_max<double>(comm_t_maxs, 0, total_conns);
    auto comm_t_p99 = get_max<double>(comm_t_p99s, 0, total_conns);
    auto comp_t_avg = get_avg<double>(comp_t_avgs, 0, total_conns);
    auto comp_t_min = get_min<double>(comp_t_mins, 0, total_conns);
    auto comp_t_max = get_max<double>(comp_t_maxs, 0, total_conns);
    auto comp_t_p99 = get_max<double>(comp_t_p99s, 0, total_conns);
    std::cout << HERMES_LOG(subsystem)
              << " n_conns=" << total_conns
              << " comm_size_avg=" << comm_size_avg << "B"
              << " comm_t_avg=" << comm_t_avg * 1e6 << "us"
              << " comm_t_p99=" << comm_t_p99 * 1e6 << "us"
              << " comm_t_min=" << comm_t_min * 1e6 << "us"
              << " comm_t_max=" << comm_t_max * 1e6 << "us"
              << " comp_t_avg=" << comp_t_avg * 1e6 << "us"
              << " comp_t_p99=" << comp_t_p99 * 1e6 << "us"
              << " comp_t_min=" << comp_t_min * 1e6 << "us"
              << " comp_t_max=" << comp_t_max * 1e6 << "us"
              << " " << usr_experiment_info
              << std::endl;
}

void print_proc_summary(const app_ctx &app, const std::vector<int> &proc_types,
                        const measurements_data &measurements, size_t proc_id, const std::string &usr_experiment_info) {
    for (auto conn_id = 0; conn_id < measurements.n_connections[proc_id]; conn_id++) {
        // the ids below are safe to use for any of the arrays in measurements_data
        auto perf_iters_start_id = measurements.get_item_storage_idx(proc_id, conn_id, measurements.warmup_iters);
        auto perf_iters_end_id = measurements.get_item_storage_idx(proc_id, conn_id, measurements.total_iters);
        auto comm_size_avg = get_avg<int>(measurements.comm_sizes, perf_iters_start_id, perf_iters_end_id);
        auto comm_t_avg = get_avg<double>(measurements.comm_times, perf_iters_start_id, perf_iters_end_id);
        auto comm_t_min = get_min<double>(measurements.comm_times, perf_iters_start_id, perf_iters_end_id);
        auto comm_t_max = get_max<double>(measurements.comm_times, perf_iters_start_id, perf_iters_end_id);
        auto comm_t_p99 = get_p99<double>(measurements.comm_times, perf_iters_start_id, perf_iters_end_id);
        auto comp_t_avg = get_avg<double>(measurements.comp_times, perf_iters_start_id, perf_iters_end_id);
        auto comp_t_min = get_min<double>(measurements.comp_times, perf_iters_start_id, perf_iters_end_id);
        auto comp_t_max = get_max<double>(measurements.comp_times, perf_iters_start_id, perf_iters_end_id);
        auto comp_t_p99 = get_p99<double>(measurements.comp_times, perf_iters_start_id, perf_iters_end_id);
        std::cout << HERMES_LOG(subsystem)
                  << "proc_id=" << proc_id
                  << " proc_type=" << app.proc_type_to_str(proc_types[proc_id])
                  << " conn_id=" << conn_id
                  << " comm_size_avg=" << comm_size_avg << "B"
                  << " comm_t_avg=" << comm_t_avg * 1e6 << "us"
                  << " comm_t_p99=" << comm_t_p99 * 1e6 << "us"
                  << " comm_t_min=" << comm_t_min * 1e6 << "us"
                  << " comm_t_max=" << comm_t_max * 1e6 << "us"
                  << " comp_t_avg=" << comp_t_avg * 1e6 << "us"
                  << " comp_t_p99=" << comp_t_p99 * 1e6 << "us"
                  << " comp_t_min=" << comp_t_min * 1e6 << "us"
                  << " comp_t_max=" << comp_t_max * 1e6 << "us"
                  << " " << usr_experiment_info
                  << std::endl;
    }
}

void write_csv(const app_ctx &app, const std::vector<int> &proc_types, const measurements_data &measurements, std::string csv_path) {
    std::ofstream csv;
    csv.open(csv_path);
    if (!csv.is_open()) {
        std::cerr << HERMES_LOG(subsystem) << "Abort. Failed to open output " << csv_path << " file" << std::endl;
        return;
    }
    std::cerr << HERMES_LOG(subsystem) << "Start writing measurements in CSV " << csv_path
              << ". Approx CSV size=" << static_cast<double>(measurements.get_size_bytes()) / 1e6 << "MB" << std::endl;
    try {
        size_t processed_conns = 0;
        csv << "proc_id,proc_type,conn_id,comm_size,comm_time,comp_time\n";
        for (auto proc_id = 0; proc_id < measurements.n_connections.size(); proc_id++) {
            for (auto conn_id = 0; conn_id < measurements.n_connections[proc_id]; conn_id++) {
                for (auto iter = 0; iter < measurements.total_iters; iter++) {
                    auto comm_size = measurements.comm_sizes[measurements.get_item_storage_idx(proc_id, processed_conns, iter)];
                    if (comm_size <= 0) {
                        std::cerr << HERMES_LOG(subsystem) << "Abort. Communication size should be >= 0" << std::endl;
                        return;
                    }
                    auto comm_time = measurements.comm_times[measurements.get_item_storage_idx(proc_id, processed_conns, iter)] * 1e6;
                    if (comm_time <= 0.0) {
                        std::cerr << HERMES_LOG(subsystem) << "Abort. Communication time should be >= 0" << std::endl;
                        return;
                    }
                    auto comp_time = measurements.comp_times[measurements.get_item_storage_idx(proc_id, processed_conns, iter)] * 1e6;
                    if (comp_time <= 0.0) {
                        std::cerr << HERMES_LOG(subsystem) << "Abort. Computation time should be >= 0" << std::endl;
                        return;
                    }
                    csv << proc_id << "," << app.proc_type_to_str(proc_types[proc_id]) << "," << conn_id << "," << comm_size << "," << comm_time << "," << comp_time << "\n";
                }
                processed_conns++;
            }
        }
    } catch (std::ofstream::failure &write_err) {
        std::cerr << HERMES_LOG(subsystem) << "Abort. Writing csv to the file " << csv_path << " failed with the exception: " << std::string(write_err.what());
    }
}

} // namespace perf

const config default_config = {
    .ts = config::TS_BACKEND_MPI_WTIME,
    .sync = config::SYNC_BACKEND_MPI_BARRIER,
    .needs_oob = true};

void init(int argc, char **argv, const config &config) {
    if (config.needs_oob) {
        oob_comm::mpi_init(argc, argv);
    }
    time::ts_init(config.ts);
    time::sync_init(config.sync);
}

void finalize() {
    if (oob_comm::my_rank != -1) {
        oob_comm::mpi_finalize();
    }
}

}; // namespace hermes
