#include "hermes.hpp"
#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include <iomanip>
#include <limits>
#include <mpi.h>
#include <string>
#include <sys/time.h>

int main(int argc, char *argv[]) {
    int result = 0;

    ::testing::InitGoogleTest(&argc, argv);
    hermes::init(argc, argv, hermes::default_config);
    result = RUN_ALL_TESTS();
    hermes::finalize();

    return result;
}

using namespace hermes::time;

namespace {
TEST(global_clock_barrier, get_global_clock_diff) {
    /*
    Run linear rtt and print the results.
    */
    // Arrange
    int N = 100;

    global_clock_barrier synchronizer(MPI_COMM_WORLD, N, global_clock_barrier::default_n_bcast_time_samples);
    int64_t result = synchronizer.get_global_clock_diff();

    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    int size;
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    std::cout << "Rank " << rank << ": " << result << std::endl;
    if (rank == 0) {
        ASSERT_EQ(result, 0);
    } else {
        // Actually, anything is possible...
    }
}
} // namespace