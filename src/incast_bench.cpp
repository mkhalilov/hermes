#include <unistd.h>

#include "cxxopts.hpp"
#include "hermes.hpp"

// Send/recv buffer feature-list:
// - ensure alignment
// - make sure that we can validate it
// - ensure that we can touch it after each iteration
// - ensure that we can ensure that they are not go into MR cache

const std::string app_subsystem = "incast_bench";

struct incast_group {
    incast_group(MPI_Comm comm, int max_flow_size) : _icomm(comm) {
        HERMES_CHKERR(MPI_Comm_rank(_icomm, &_icomm_my_rank), hermes::oob_comm::ret_success, hermes::oob_comm::mpi_err_handler, app_subsystem);
        HERMES_CHKERR(MPI_Comm_size(_icomm, &_icomm_size), hermes::oob_comm::ret_success, hermes::oob_comm::mpi_err_handler, app_subsystem);
        _n_remote_ranks = _icomm_my_rank == 0 ? (_icomm_size - 1) : 1;
        _requests.resize(_n_remote_ranks);
        if (_icomm_my_rank == 0) { // root
            _recv_bufs.resize(_n_remote_ranks);
            for (auto i = 0; i < _n_remote_ranks; i++) {
                _recv_bufs[i].resize(max_flow_size);
            }
        } else {
            _send_buf.resize(max_flow_size);
        }
        std::cout << HERMES_LOG(std::string("incast_group"))
                  << " initialized incast comm ctx"
                  << " my_rank=" << _icomm_my_rank
                  << " n_remote_ranks=" << _n_remote_ranks
                  << " max_flow_size=" << max_flow_size
                  << std::endl;
    }

    bool is_root() const {
        return _icomm_my_rank == 0 ? true : false;
    }

    size_t get_remote_participants_number() const {
        return _n_remote_ranks;
    }

    void post_participant(int conn_id, int flow_size) {
        if (is_root()) {
            HERMES_CHKERR(MPI_Irecv(_recv_bufs[conn_id].data(), flow_size, MPI_CHAR, conn_id + 1, _tag_start_id + conn_id + 1, _icomm, &_requests[conn_id]),
                          hermes::oob_comm::ret_success, hermes::oob_comm::mpi_err_handler, app_subsystem);
        } else {
            HERMES_CHKERR(MPI_Isend(_send_buf.data(), flow_size, MPI_CHAR, 0, _tag_start_id + _icomm_my_rank, _icomm, &_requests[0]),
                          hermes::oob_comm::ret_success, hermes::oob_comm::mpi_err_handler, app_subsystem);
        }
    }

    int wait_participant() {
        int conn_id;
        MPI_Status status;
        HERMES_CHKERR(MPI_Waitany(_n_remote_ranks, _requests.data(), &conn_id, &status),
                      hermes::oob_comm::ret_success, hermes::oob_comm::mpi_err_handler, app_subsystem);
        return conn_id;
    }

    MPI_Comm _icomm;
    int _icomm_my_rank;
    int _icomm_size;
    int _n_remote_ranks;
    std::vector<char> _send_buf;
    std::vector<std::vector<char>> _recv_bufs;
    std::vector<MPI_Request> _requests;
    const int _tag_start_id = 42;
};

struct incast_simple_app : public hermes::app_ctx {
    incast_simple_app(int max_flow_size) {
        if (!hermes::oob_comm::is_initialized()) {
            std::cerr << HERMES_LOG(app_subsystem) << "This application depends on the initialized OOB communication context" << std::endl;
            exit(EXIT_FAILURE);
        }
        HERMES_CHKERR(MPI_Comm_dup(MPI_COMM_WORLD, &_icomm), hermes::oob_comm::ret_success, hermes::oob_comm::mpi_err_handler, app_subsystem);
        _igroup = new incast_group(_icomm, max_flow_size);
    }

    ~incast_simple_app() {
        HERMES_CHKERR(MPI_Comm_free(&_icomm), hermes::oob_comm::ret_success, hermes::oob_comm::mpi_err_handler, app_subsystem);
        delete _igroup;
    }

    void post_nb_comm(int conn_id, int flow_size) {
        _igroup->post_participant(conn_id, flow_size);
    }

    int wait_nb_comm() {
        return _igroup->wait_participant();
    }

    void do_comp(double time) {
        // do nothing
    }

    size_t get_conn_number() const {
        return _igroup->get_remote_participants_number();
    }

    std::string get_name() const {
        return "incast_simple_app";
    }

    int get_proc_type() const {
        return _igroup->is_root() ? 0 : 1;
    }

    std::string proc_type_to_str(int type) const {
        return type ? "leaf" : "root";
    }

    MPI_Comm _icomm;
    incast_group *_igroup;
};

struct incast_mp_root_app : public hermes::app_ctx {
    incast_mp_root_app(int max_flow_size, std::string root_node_hostname) {
        if (!hermes::oob_comm::is_initialized()) {
            exit(EXIT_FAILURE);
        }
        char my_hostname_c_str[_SC_HOST_NAME_MAX];
        int hostname_len = _SC_HOST_NAME_MAX;
        if (gethostname(my_hostname_c_str, hostname_len)) {
            std::cerr << HERMES_LOG(app_subsystem) << "failed to get hostname" << std::endl;
            exit(EXIT_FAILURE);
        }
        std::string my_hostname(my_hostname_c_str);

        // Root ranks reside on the host with a user-provided hostname.
        // To count the number of leafs/roots we'll do a comm split with a rank type as a color
        int type_comm_size, type_comm_rank;
        int is_root = my_hostname == root_node_hostname ? 1 : 0;
        MPI_Comm type_comm;
        HERMES_CHKERR(MPI_Comm_split(MPI_COMM_WORLD, is_root, 0, &type_comm),
                      hermes::oob_comm::ret_success, hermes::oob_comm::mpi_err_handler, app_subsystem);
        HERMES_CHKERR(MPI_Comm_size(type_comm, &type_comm_size),
                      hermes::oob_comm::ret_success, hermes::oob_comm::mpi_err_handler, app_subsystem);
        HERMES_CHKERR(MPI_Comm_rank(type_comm, &type_comm_rank),
                      hermes::oob_comm::ret_success, hermes::oob_comm::mpi_err_handler, app_subsystem);
        int n_incast_roots = 0;
        HERMES_CHKERR(MPI_Allreduce(&is_root, &n_incast_roots, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD),
                      hermes::oob_comm::ret_success, hermes::oob_comm::mpi_err_handler, app_subsystem); // all roots will commit with 1
        if (n_incast_roots == 0) {
            std::cerr << HERMES_LOG(app_subsystem) << "number of incast roots must be positive" << std::endl;
            exit(EXIT_FAILURE);
        }
        // We'll split comm_world into the n_incast_roots concurrent incast groups (_icomm)
        // The color of the _icomm is the rank of its root in the root type_comm
        int my_icomm_id, rank_id, n_incast_leafs = -1, n_leafs_per_icomm = -1;
        if (is_root) {
            my_icomm_id = type_comm_rank;
        } else {
            n_incast_leafs = type_comm_size;
            if (n_incast_roots > n_incast_leafs) {
                std::cerr << HERMES_LOG(app_subsystem) << "n_incast_roots=" << n_incast_roots
                          << " and is larger n_incast_leafs=" << n_incast_leafs << std::endl;
                exit(EXIT_FAILURE);
            }
            n_leafs_per_icomm = n_incast_leafs / n_incast_roots;
            for (rank_id = n_leafs_per_icomm, my_icomm_id = 0; rank_id <= n_incast_leafs; rank_id += n_leafs_per_icomm, my_icomm_id++) {
                if (type_comm_rank < rank_id) {
                    break;
                }
            }
            if (my_icomm_id == n_incast_roots) {
                my_icomm_id--;
            } else if (my_icomm_id > n_incast_roots) {
                std::cerr << "it's a bug in case you are here" << std::endl;
                exit(EXIT_FAILURE);
            }
        }
        HERMES_CHKERR(MPI_Comm_split(MPI_COMM_WORLD, my_icomm_id, is_root ? 0 : 1, &_icomm),
                      hermes::oob_comm::ret_success, hermes::oob_comm::mpi_err_handler, app_subsystem);
        std::cout << HERMES_LOG(app_subsystem)
                  << "finished _icomm creation:"
                  << " my_hostname=" << my_hostname
                  << " my_world_rank=" << hermes::oob_comm::my_rank
                  << " is_root=" << is_root
                  << " type_comm_rank=" << type_comm_rank
                  << " n_incast_roots=" << n_incast_roots
                  << " n_incast_leafs=" << n_incast_leafs
                  << " n_leafs_per_comm=" << n_leafs_per_icomm
                  << " _my_icomm_id=" << my_icomm_id
                  << std::endl;
        _igroup = new incast_group(_icomm, max_flow_size);
        _my_type.is_root = is_root;
        _my_type.icomm_id = my_icomm_id;
        HERMES_CHKERR(MPI_Comm_free(&type_comm), // we don't need this communicator anymore
                      hermes::oob_comm::ret_success, hermes::oob_comm::mpi_err_handler, app_subsystem);
    }

    ~incast_mp_root_app() {
        HERMES_CHKERR(MPI_Comm_free(&_icomm),
                      hermes::oob_comm::ret_success, hermes::oob_comm::mpi_err_handler, app_subsystem);
        delete _igroup;
    }

    void post_nb_comm(int conn_id, int flow_size) {
        _igroup->post_participant(conn_id, flow_size);
    }

    int wait_nb_comm() {
        return _igroup->wait_participant();
    }

    void do_comp(double time) {
        // do nothing
    }

    size_t get_conn_number() const {
        return _igroup->get_remote_participants_number();
    }

    std::string get_name() const {
        return "incast_mp_root_app";
    }

    union participant_type {
        struct {
            int is_root : 1;
            int icomm_id : 31;
        };
        int raw;
    } _my_type;

    int get_proc_type() const {
        return _my_type.raw;
    }

    std::string proc_type_to_str(int type) const {
        participant_type tmp;
        tmp.raw = type;
        return std::string(tmp.is_root ? "root_" : "leaf_") + std::to_string(tmp.icomm_id);
    }

    MPI_Comm _icomm;
    incast_group *_igroup;
};

const std::string invalid_string_opt_val = "invalid-value-0xdeadbeaf";

void opts_parse(int argc, char **argv, cxxopts::ParseResult &opts) {
    cxxopts::Options options("bmark", "uber benchmark");
    options.add_options()
        // Transport parameters
        ("S,flow_size", "Incast leaf flow size", cxxopts::value<int>()->default_value("262144"))                                  //
        ("n,iters", "Number of benchmark iterations, N", cxxopts::value<std::size_t>()->default_value("10"))                      //
        ("w,warmup", "Number of warmup iterations, N", cxxopts::value<std::size_t>()->default_value("10"))                        //
        ("o,csv_path", "CSV output path", cxxopts::value<std::string>()->default_value(invalid_string_opt_val))                   //
        ("a,app", "Incast application to use, <simple,mp_root>", cxxopts::value<std::string>()->default_value("simple"))          //
        ("R,root_hostname", "Root node for mp_root incast", cxxopts::value<std::string>()->default_value(invalid_string_opt_val)) //
        ("h,help", "Print usage");
    opts = options.parse(argc, argv);
    if (opts.count("help")) {
        std::cout << options.help() << std::endl;
        exit(0);
    }
}

int main(int argc, char **argv) {
    cxxopts::ParseResult opts;
    opts_parse(argc, argv, opts);
    hermes::init(argc, argv, hermes::default_config);
    {
        hermes::app_ctx *app;
        auto flow_size = opts["flow_size"].as<int>();
        if (opts["app"].as<std::string>() == "simple") {
            app = new incast_simple_app(flow_size);
        } else if (opts["app"].as<std::string>() == "mp_root") {
            auto root_node = opts["root_hostname"].as<std::string>();
            if (root_node == invalid_string_opt_val) {
                std::cerr << HERMES_LOG(app_subsystem) << "root node must be set for the mp_root app" << std::endl;
                exit(EXIT_FAILURE);
            }
            app = new incast_mp_root_app(flow_size, root_node);
        } else {
            std::cerr << HERMES_LOG(app_subsystem) << "unknown app type requested" << std::endl;
            exit(EXIT_FAILURE);
        }
        hermes::measurements_data local_perf_data(app->get_conn_number(), opts["warmup"].as<std::size_t>(), opts["iters"].as<std::size_t>(), flow_size, 0.0);
        hermes::perf::eval_nb_loop(*app, local_perf_data);
        std::vector<int> proc_types = app->get_all_participants_proc_types(0, MPI_COMM_WORLD);
        hermes::measurements_data global_perf_data(local_perf_data, 0, MPI_COMM_WORLD);
        if (hermes::oob_comm::my_rank == 0) {
            for (auto proc_id = 0; proc_id < hermes::oob_comm::world_size; proc_id++) {
                hermes::perf::print_proc_summary(*app, proc_types, global_perf_data, proc_id, "my_incast_experiment");
            }
            if (opts["csv_path"].as<std::string>() != invalid_string_opt_val) {
                hermes::perf::write_csv(*app, proc_types, global_perf_data, opts["csv_path"].as<std::string>());
            }
        }
        delete app;
    }
    hermes::finalize();
    return 0;
}
