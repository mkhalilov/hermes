# Syncronizer

Please see the header file.

## 1. Measuring the Difference Between Two Nodes

What we measure is indeed the clock differences between two nodes, not the communication time.
That means, we measure simultaneously (ignoring Einstein's theorem) the same time and record the differences.
But how?

We make an assumption here.
Time that `MPI_Recv` takes is equal to the time `MPI_Send` takes.
So, we end up with this diagram.
The diagram specifies the calls from the `Root Node` side, e.g. `MPI_Send` means Root Node sends data.

```mermaid
sequenceDiagram
    participant Root as Root Node
    participant Other as Other Node

    Root->>+Other: MPI_Send
    Note over Other: t_other = Measure current local time of the other node
    Note over Root: t_avg = Average local time of the root node
    Other->>-Root: MPI_Recv

```

In the code, we calculate this equation:

```
local_clock_difference = t_avg - t_other
```

Since `MPI_Send` and `MPI_Recv` take the same amount of time according to our assumption, `t_avg` gives you the local clock of the root node when `t_other` happens in the other node.

## 1. Tree Based Algorithm

### 1. Divide processes into 2 groups

Divide processes into 2 groups.

- Group 1: number of processes which is exactly `2^k` for some natural number `k`.
- Group 2: remaning processes (if any).

Example: we have 13 nodes, and we want to synchronize the clock.
The first `2^3=8` (`k=3`) processes are in the group 1.
The rest is in group 2.

More precisely:

```python3
group_1 = [0,1,2,3,4,5,6,7]
group_2 = [8,9,10,11,12]
```

### 2. Do syncronization on the tree

Match each processes in each step, and continue with synchronizing.
This means, we do log2(P) synchronization steps.
Moreover, at each step we synchronize all nodes if they are not finished yet.
This example gives a trace:

| Step       | 0      | 1      | 2      | 3      | 4      | 5      | 6      | 7      |
| ---------- | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| **Step 1** | client | server | client | server | client | server | client | server |
| **Step 2** | client | break  | server | break  | client | break  | server | break  |
| **Step 3** | client |        | break  |        | server |        | break  |        |
| **Step 4** | client |        |        |        | break  |        |        |        |

Client means node behaves like a client (gets the difference data).
Server means vice versa.

At step 4, client 0 has all the `diff` data to decide on the clocks.
It can simply distribute this information on all clients.

The information flow follows:
Step 1:

```txt
1 sends information to 0
3 sends information to 2
5 sends information to 4
7 sends information to 6
```

Step 2:

```txt
2 sends information to 0
6 sends information to 4
```

Step 3:

```txt
4 sends information to 0
```

This pattern can be implemented using the following equations:

```latex
# k = step we are at (from k=1 to k=log(P))
# r = rank of a node

(r mod 2^k = 0) => r is a client
# example 1: k = 1, rank 1: 1 mod 2^1 != 0 which means it is not a client at step k=1
# example 2: k = 0, rank 0: 0 mod 2^1 == 0 which means it is a client at step k=1

r mod 2^k => 2^(k-1)
# this means actually, assume these are the servers: m*2^(k-1) where m is in natural numbers, and when m*2^(k-1) < P
# example 1: k = 1, rank 1: 1 mod 2^1  == 2^(k-1) which means it is a server


# there are other nodes which are neither server, nor client
# those nodes are simply the ones which finished the processing for stage 1
# those nodes (also shown as break in the table) do not contribute to communication

```

### 3. Group 2

When we synchronized group 1, we already know the time difference of each node relative to the root node.
However, we also have nodes in group 2 (if we have group 2).
Group 2 simply represents the processes which are basically the remainder from the first group tree.
Since putting them into the first group tree makes tree unbalanced, we prefer to handle them at the later stage.

The idea is simple.
Since the number of processes in group 2 is less than number of processes in group 1 (otherwise we would have chosen a different group1/group2 partitioning and include the nodes in group 1), we can simply match each process in group 1 and group 2 so that they can share the differences.

For node size of 13, we can use this table for example (or any other partitioning):

| Step          | 0      | 1      | 2      | 3      | 4      | 5      | 6       | 7       | 8      | 9      | 10     | 11     | 12     | 13     |
| ------------- | ------ | ------ | ------ | ------ | ------ | ------ | ------- | ------- | ------ | ------ | ------ | ------ | ------ | ------ |
| **Step Last** | server | server | server | server | server | server | nothing | nothing | client | client | client | client | client | client |

The idea is: nodes from 0 to 7 know their relative difference to the root node.
If we can synchronize nodes [0-5] with [8-13], we can simply find the differences in [8-13].
More clearly, we send the current difference from 0 to 8, and 8 also knows the clock cycles difference between 0 and 8, and it can calculate the clock difference.

This can be done in parallel for all nodes in 1 step only.

After this step, every node knows its difference relative to the root node.
However, this does not mean they can easily start when they know this difference.
This is because, they don't know how much each node takes after a barrier operation which will be explained in the next sections.

## 2. Finding start times

To make every node start at the same time, we need to learn at exactly which point (in terms of local clock cyles) the operation starts.
For example, node 1 says now is 100 cycles, and node 2 says now is 115 cycles (there is a 15 cycles difference between the clocks due to hardware etc.).
This 15 difference was already found in the first step using either tree or linear tests.
However, when we call `MPI_Barrier`, node 0 would finish the operation at cycle 115 (in local clock) and node 1 would finish the operation at cycle 120 (in local local).
That means, there is a delay of 15 cycles for node 0, but only 5 cycles for node 1.
To compansate this delay, this step aims to measure the time which would be simultaneous for all processes.
In this particular example, how to make the `MPI_Barrier` finish at 115. cycle for node 0 and 130. cycle for node 1 (in local times both).
This is because, (115,130) means they are actually simultaneous (TODO i'm confused here because of the Einstein's thoerem. because he basically changed the definition of simulatenous. so, i'm not sure if it is really relevant here. but I feel somehow it is relevant)

## 3. Block until everything starts

After we have accurate time measurements, we set a late time where each node waits until.
This makes sure that all nodes can at least achieve that point.
For example, we set the node 0 until cycle 150 and node 1 until cycle 165 (in local clocks, and these numbers use the example from previous section),
Since going to kernel is not precise, we keep CPU idle continuosly by checking the clocks.
Like this:

```C
while(!are_we_there_yet());

// return from the benchmarking code
```
