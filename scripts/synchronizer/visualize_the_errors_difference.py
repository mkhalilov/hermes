# Plot the results from SyncrhonizerTest with slow network
import numpy as np
import matplotlib.pyplot as plt

number_of_tries = 10
num_nodes = 3

with open("build/output/performance_logs_fast_interface.txt", "r", encoding="utf-8") as f:
    lines = f.readlines()

    mpi_barrier_times = [[0 for _ in range(num_nodes)] for _ in range(number_of_tries)]
    custom_barrier_times = [[0 for _ in range(num_nodes)] for _ in range(number_of_tries)]
    no_barrier_times = [[0 for _ in range(num_nodes)] for _ in range(number_of_tries)]
    for line in lines:
        line = line.rstrip()

        if "end" in line:
            break
        if "start" in line:
            continue
        if "#" in line:
            continue
        time, when, rank, test_type, order = line.split(",")
        time = float(time)
        rank = int(rank)
        test_type = int(test_type)
        order = int(order)

        if when == "after":
            if test_type == 1:
                mpi_barrier_times[order][rank] = time

            elif test_type == 0:
                custom_barrier_times[order][rank] = time

            elif test_type == 2:
                no_barrier_times[order][rank] = time

    # Compute the L1 error
    mpi_barrier_times = np.array(mpi_barrier_times)
    custom_barrier_times = np.array(custom_barrier_times)
    no_barrier_times = np.array(no_barrier_times)
    mpi_l1 = abs(mpi_barrier_times.mean(axis=1).reshape(number_of_tries, 1) - mpi_barrier_times).mean(axis=1)
    custom_l1 = abs(custom_barrier_times.mean(axis=1).reshape(number_of_tries, 1) - custom_barrier_times).mean(axis=1)
    no_barrier_l1 = abs(no_barrier_times.mean(axis=1).reshape(number_of_tries, 1) - no_barrier_times).mean(axis=1)

    # print(mpi_l1)
    # print(mpi_barrier_times)

    if (abs(mpi_barrier_times) < 1e-3).any():
        raise ValueError("Some data is missing!")

    plt.figure(figsize=(16, 9))
    plt.hist(mpi_l1, bins=10, label="MPI Barrier", alpha=0.7)
    plt.hist(custom_l1, bins=10, label="Custom Barrier", alpha=0.7)
    plt.hist(no_barrier_l1, bins=10, label="No Barrier (sanity check)", alpha=0.7)
    # plt.hist([[1, 2, 3], [4, 5, 6], [7, 8, 9]], bins=3)
    plt.legend()
    plt.xlabel("L1 Error")
    plt.ylabel("Number of Occurences of this Error")
    plt.title("histogram(abs(mean(times) - times)) where times is [node1_barrier_exit_time, node2_time_barrier_exit_time, ...]", size=10)
    plt.suptitle("L1 Error (microseconds) Histogram")
    plt.savefig("l1_error.png")
