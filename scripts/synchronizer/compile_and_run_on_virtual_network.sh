# I have a test network with 1 router, 3 hosts
# This one is to compile, send and run the tests
# Run this in the root directory

read -p "This will remove the build directory. Please press enter to continue (or CTRL+C to cancel)"

# Source the intel MPI environment
source /opt/intel/oneapi/setvars.sh

# Update the source
git pull

# Compile
rm -rf build
mkdir build
cd build
cmake ..
make -j4
cd ..

# Send
ips=("192.168.124.95" "192.168.124.166")
for ip in "${ips[@]}"; do
    ssh $ip "rm -rf hermes && mkdir -p hermes/build"
    scp build/googletest $ip:hermes/build
done

# Run and record the output
I_MPI_HYDRA_IFACE=enp1s0 ICX_TLS=tcp mpiexec -machinefile ~/machinefile -hostfile ~/hostfile -n 6 $(pwd)/build/test_synchronizer &>test_output.txt
