/*
This server is used to communicate the timestamp messages so that we can measure our code's correctness.
*/
#include <arpa/inet.h>
#include <mpi.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#define PORT 8080
#define BUFFER_SIZE 1024

// Function to get the current timestamp as a string
long long get_current_time() {
    // static char buffer[64];
    // time_t now = time(NULL);
    // struct tm *tm_info = localtime(&now);
    // strftime(buffer, sizeof(buffer), "%Y-%m-%d %H:%M:%S", tm_info);
    // return buffer;
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return (long long)tv.tv_sec * 1000000 + tv.tv_usec;
    return MPI_Wtime();
}

FILE *fp;
pthread_mutex_t lock;

// Function to handle communication with a single client
void *handle_client(void *arg) {
    int client_socket = *(int *)arg;
    char buffer[BUFFER_SIZE];
    int bytes_received;

    while ((bytes_received = recv(client_socket, buffer, sizeof(buffer) - 1, 0)) > 0) {
        buffer[bytes_received] = '\0'; // Null-terminate the received string
        for (int i = 0; i < bytes_received; i++) {
            if (buffer[i] == '\n') {
                buffer[i] = '\0';
                break;
            }
        }
        long long current_time = get_current_time();
        pthread_mutex_lock(&lock);
        printf("Got message: %s at %lld\n", buffer, current_time);
        fprintf(fp, "%lld,%s\n", current_time, buffer);
        // printf("%lld,%s\n", current_time, buffer);
        pthread_mutex_unlock(&lock);
        // Optionally, you can send an acknowledgment back to the client
        // send(client_socket, "Message received", 17, 0);
        if (strcmp(buffer, "end") == 0) {
            break;
        }
        if (strcmp(buffer, "end_all") == 0) {
            close(client_socket);
            fclose(fp);
            exit(0);
        }
    }
    close(client_socket);
    while (1) {
        sleep(1);
    }
}

int main() {
    fp = fopen("build/output/performance_logs_fast_interface.txt", "w");
    int server_socket, client_socket;
    struct sockaddr_in server_addr, client_addr;
    socklen_t client_addr_len = sizeof(client_addr);
    pthread_t thread_id;

    // Create socket
    server_socket = socket(AF_INET, SOCK_STREAM, 0);
    if (server_socket < 0) {
        perror("socket");
        exit(EXIT_FAILURE);
    }

    // Set up server address
    memset(&server_addr, 0, sizeof(server_addr));
    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = INADDR_ANY;
    server_addr.sin_port = htons(PORT);

    // Bind socket
    if (bind(server_socket, (struct sockaddr *)&server_addr, sizeof(server_addr)) < 0) {
        perror("bind");
        close(server_socket);
        exit(EXIT_FAILURE);
    }

    // Listen for incoming connections
    if (listen(server_socket, 5) < 0) {
        perror("listen");
        close(server_socket);
        exit(EXIT_FAILURE);
    }

    printf("Server listening on port %d\n", PORT);

    // Main loop to accept and handle clients
    while (1) {
        client_socket = accept(server_socket, (struct sockaddr *)&client_addr, &client_addr_len);
        if (client_socket < 0) {
            perror("accept");
            continue;
        }

        // printf("Accepted connection from %s:%d\n",
        //        inet_ntoa(client_addr.sin_addr),
        //        ntohs(client_addr.sin_port));

        // Create a thread to handle the client
        if (pthread_create(&thread_id, NULL, handle_client, &client_socket) != 0) {
            perror("pthread_create");
            close(client_socket);
        } else {
            pthread_detach(thread_id); // Automatically reclaim thread resources
        }
    }

    close(server_socket);
    return 0;
}
